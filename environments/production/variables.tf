variable "region" {
  default = "us-east-1"
}

variable "environment" {
    type = string
    description = "name of environment"
}

variable "network_name" {
    type = string
    description = "name of network"
}